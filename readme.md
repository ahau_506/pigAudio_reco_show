<!--
 * @Author: qiaoyu
 * @Date: 2022-08-19 13:27:28
 * @LastEditTime: 2022-09-04 13:24:55
 * @LastEditors: qiaoyu
 * @Description: 
 * @FilePath: \pigAudio_reco_show\readme.md
 * Made by qiaoyukeji@gmail.com
-->
### 生猪音频在线实时分类检测系统
本项目由实验室前期提取的不同类型生猪音频数据，在百度 EasyDL 进行训练后对外提供音频分类服务，经过测试效果良好。

本仓库保存 生猪音频在线实时分类检测系统 的前端展示界面。

![](./img/show001.jpg)

可在 js > index.js > 第54行修改自己训练完成后 EasyDL 提供的 api 地址。
